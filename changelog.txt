v 0.8.9.6 (23 jul)

x10 link for Sacrifice Unicorns button

v 0.8.9.5 (22 jul)

Alloy Warehouses
Concrete Warehouses
Concrete Barns
Concrete Huts

v 0.8.8.5

Concrete (T3 craft recipe)

Spiders (trade scaffolds for coal)
Factory (+5% production rate)

Mechanization, Metallurgy and Combustion techs

v 0.8.7.5

Electrolytic Smelting (+95% to smelter effectiveness)

Iron output of smelters is now affected by Solar Revolution bonus
Fixed disabled observatories still giving chance bonus to events
Fixed Sky Palace not giving correct bonuses

v 0.8.6.5

Observatories are now togglable
Steamworks is not tunable anymore (yet togglable)

Sky Palace (endgame Ziggurat upgrade)
Templars (endgame religion upgrade)

Pumpjack - improves oil production by 75%

Two new achievements

v 0.8.5.5

Added "Trade all" option
Added "High precision" game setting ( 3 digits after point )
Removed "manpower" to "catpower" and "compedium" to "compendium"

v 0.8.5.4

changed by Zusias:

Added %power toggle for automated structures
____

Removed leather from the resources

v 0.8.4.4

Time to cap in the resource tooltips

v 0.8.4.3

Fixed Sunsipre not giving faith bonus
Fixed Ziggurat upgrades not recalculating prices

Barges - improve coal limits for Harbors
Advanced Automation - makes Steamworks activate twice per year

Paragon levels should give minor bonus to the resource limit

+ pile of placeholder techs

v 0.8.3.2

Drama and Poetry tech
Cultural Festivals

Fixed steamworks giving effect even without the oil

v 0.8.2.2

Geodesy will now give ~60% boost to the geologists coal output ( 0.0225 cpt, 0.0008 gpt)

v 0.8.2.1

Craftable blueprints (25 compendium, 25k science)

v 0.8.2.0

Titanium Saw
Alloy Saw

Magneto
Biolab

Industrialization

v 0.8.1.0

Alloy barns
Alloy axe

All barn capacity upgrades will now affect catnip as well if Silos are researched (25% of ratio)

v 0.8.0.0

Fixed few more memory leaks (Thanks Zusias!)

Craft table will display price tooltips for recipes
Renamed "Diplomacy" tab to "Trade"
Zebras will display titanium drop
Buy and Sell sections will be now color coded
Certain important messages in the log are now color coded

Nerfed Mints to the ground (less furs, significanly less ivory)
Fixed Titanium Warehouses not being correctly unlocked

v 0.7.9.9

Fixed lots of memory leaks in the game.

v 0.7.9.8

Silos (workshop upgrade), +750 max catnip to warehouse capacity

v 0.7.8.8

Basilica (religion upgrade for culture)
Acoustics (tech, 65K science, 65 compedium, unlocks Chapel)
Chapel ( + 200 to max culture, +0.05 culture per tick, +0.005 faith per tick )

v 0.7.7.7

Fixed resource table ( tooltips should work in chrome now)

v 0.7.7.6

Resources will stay if limit is lower for some reason. (Workaround for a couple of cases).

Titanium Barns
Titanium Warehouses

v 0.7.6.6

changed by Duke:

"Send all" command will now display single message instead of spamming the log
Fixed religion bonuses working incorrectly
_____

Added progress indicator for Solar Revolution

v 0.7.6.5

Mint
Updated data import to support some browsers

v 0.7.5.4

Renamed Archeology to the Geology (makes more sense)

- Priest (0.0015 faith per tick (that is actually a lot)) [Theology]
- Geologist (0.015 coal per tick) [Geology]

v 0.7.4.4

Middle column is now fixed and scrollable

v 0.7.4.3

Cargo Ships (Every ship you have will give 2% to Harbor resource limits)	//will give diminishing effect starting with 187% total
Every observatory will now give a 1% chance of auto-generating starcharts

v 0.7.3.3

Increased compedium prices of all techs

Architecture (43k science, 10 compediums)
Mansion (200 slab, 100 steel, 1 titanium), +1 max kitten

Added one Unethical Achievement.

v 0.7.2.3

Solar Revolution
Alloy

Fixed religion buttons always visible regardless of the faith pool

v 0.7.1.3

Ivory Citadel

Unicorn Rifts
Ivory Meteors

Ziggurats will require 5% of resources to unlock instead of 30%

v 0.7.0.3

Red prices in tolltips
Alert for catnip craft

v 0.7.0.2

changed by Zusias:

Pastures, Tradeposts and Amphitheatres will now give correct reduction effects with smother distribution curve.
_

Rebalanced ratio coefficients to grant the same distribution curve.
Rebalanced prices of Harbors and Megaliths in favour of slabs

v 0.7.0.1

Every compedium will give a +10 bonus to the max science
Raised science price of compediums from 5K to 10K

Paragon points will improve your total production by 1%

Zebras are now trading slabs to iron/plates instead of plates to coal/steel

v 0.7.0.0

Calciner: 
Converts minerals and oil into iron and titanium [chemistry] 
(120 stell, 5 titanium, 5 bp, 300 oil)
-1.5 min, -0.012 oil, +0.001 titanium, +0.15 iron

Changed Oil Well price to (50 steel, 25 gear, 25 scaffold)

v 0.6.9.6

Reduced Observatory price ratio from 1.15 to 1.10 and increased Slab price from 15 to 35
Reduced Trade Ship scaffold price from 120 to 100

Changed prices of Quarry to 50 scaffold, 150 steel, 1000 slab (prices may be reduced later)

Steel Saw: improves lumber mull efffectiveness by 10%

v.0.6.8.6

Raised base faith output of the temples by 50%
Trade ships will give 0.35% to the titanium proc instead of 0.035%
Improved base titanium output from 1 to 1.5
Reduced manuscripts culture price from 500 to 400
Reduced manuscript prices of Astronomy and Navigation to 65 and 100 (from 75 and 150)

v.0.6.8.5

Religion: Golden Spire (+50% max faith per temple)
Religion: Sun Altar (+0.5% happiness per temple)
Religion: Stained Glass (doubles temple culture)

Production buildings group was split into Production and Storage

Quarry (50 Scaffold, 1000 Slab), +35% minerals ratio, +0.015 coal per tick [archeology]

Oil Well (25 Scaffold, 100 Steel), 2500 max oil, +0.01 oil per tick [chemistry]

'Tired' status will be cleared after very brief timeout
Pritning press will give 0.00025 manuscript per tick (2 per year total) constantly instead of 0.5 per year

Fixed astrolabe effect working even without purchased upgrade (not a nerf)
Reduced titanum price of Titanium Reflectors from 25 to 15

Craft resources will be highlighted for buildings

v.0.6.7.5

Trade ships will give 0.35% to the titanium drop chance
Added Hunt All button
Raised Mines coal output from 0.01 to 0.03
Added color codes to craft resources

v.0.6.7.4

changed by Zusias:

Census skills wlll now contribute to the production rate
____

Hunting armor was split into two upgrades
Bolas and Hunting Armor will increase mapowerMax to 400 and 1000 in ironwill
